/*
 * 用于关联github.com/RichardKnop/machinery/v2/brokers/redis包中的函数和方法
 */
package redis

import (
	"github.com/RichardKnop/machinery/v2/brokers/iface"
	_ "github.com/RichardKnop/machinery/v2/brokers/redis"
	"github.com/RichardKnop/machinery/v2/common"
	"github.com/RichardKnop/machinery/v2/config"
	"github.com/RichardKnop/machinery/v2/tasks"
	"github.com/go-redis/redis/v8"
	"github.com/go-redsync/redsync/v4"
	"sync"
	_ "unsafe"
)

type BrokerGR struct {
	common.Broker
	rclient              redis.UniversalClient
	consumingWG          sync.WaitGroup // wait group to make sure whole consumption completes
	processingWG         sync.WaitGroup // use wait group to make sure task processing completes
	delayedWG            sync.WaitGroup
	socketPath           string // If set, path to a socket file overrides hostname
	redsync              *redsync.Redsync
	redisOnce            sync.Once
	redisDelayedTasksKey string
}

//go:linkname (*BrokerGR).nextTask github.com/RichardKnop/machinery/v2/brokers/redis.(*BrokerGR).nextTask
func (b *BrokerGR) nextTask(queue string) (result []byte, err error)

//go:linkname (*BrokerGR).nextDelayedTask github.com/RichardKnop/machinery/v2/brokers/redis.(*BrokerGR).nextDelayedTask
func (b *BrokerGR) nextDelayedTask(key string) (result []byte, err error)

//go:linkname (*BrokerGR).GetPendingTasks github.com/RichardKnop/machinery/v2/brokers/redis.(*BrokerGR).GetPendingTasks
func (b *BrokerGR) GetPendingTasks(queue string) ([]*tasks.Signature, error)

//go:linkname (*BrokerGR).GetDelayedTasks github.com/RichardKnop/machinery/v2/brokers/redis.(*BrokerGR).GetDelayedTasks
func (b *BrokerGR) GetDelayedTasks() ([]*tasks.Signature, error)

//go:linkname (*BrokerGR).consume github.com/RichardKnop/machinery/v2/brokers/redis.(*BrokerGR).consume
func (b *BrokerGR) consume(deliveries <-chan []byte, concurrency int, taskProcessor iface.TaskProcessor) error

//go:linkname (*BrokerGR).consumeOne github.com/RichardKnop/machinery/v2/brokers/redis.(*BrokerGR).consumeOne
func (b *BrokerGR) consumeOne(delivery []byte, taskProcessor iface.TaskProcessor) error

//go:linkname getQueueGR github.com/RichardKnop/machinery/v2/brokers/redis.getQueueGR
func getQueueGR(config *config.Config, taskProcessor iface.TaskProcessor) string
