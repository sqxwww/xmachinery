package redis

type Option func(broadcast *BrokerBroadcast)

func WithRedisBroadcastTasksKey(key string) Option {
	return func(b *BrokerBroadcast) {
		b.redisBroadcastTasksKey = key
	}
}

func WithBroadcastHeaderKey(key string) Option {
	return func(b *BrokerBroadcast) {
		b.broadcastHeaderKey = key
	}
}
