module gitee.com/sqxwww/xmachinery

go 1.13

require (
	github.com/RichardKnop/machinery/v2 v2.0.10
	github.com/go-redis/redis/v8 v8.6.0
	github.com/go-redsync/redsync/v4 v4.0.4
	github.com/google/uuid v1.2.0
	github.com/robfig/cron/v3 v3.0.1
)
